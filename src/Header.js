import React, { Component } from 'react';
import styled from 'styled-components'
import { observer, inject } from 'mobx-react'
import { computed,  } from 'mobx'
import { NavLink } from 'react-router-dom'
import { SwitchHorizontalIcon, ScaleIcon, DatabaseIcon, UserCircleIcon,
} from '@heroicons/react/outline'

import { IconDashboard,  } from './Icons'

import { useLocation } from 'react-router-dom'
import Body from './Components/Body'
import { withRouter } from "react-router-dom";

function HeaderExpand(props) {
	const location = useLocation();
	return <SuperHeader 
			active={location.pathname === "/" ? true : false}
			
			>{props.children}</SuperHeader>
}

@inject('store')
@observer
class SidebarCompontent extends Component {

	constructor(props) {
		super(props);
		if(this.props.location.pathname === "/signup"){
			this.props.history.push('/')
		}
		if(this.props.location.pathname === "/login"){
			this.props.history.push('/')
		}
		
	}
	componentDidMount(){
		document.addEventListener('keydown',this.shortcutHandler);
	}
	componentWillUnmount(){
		document.removeEventListener('keydown',this.shortcutHandler);
	}
	shortcutHandler = e => {
		if(e.keyCode===75 && e.ctrlKey){
			e.preventDefault();
			// select all text in input with id q
			document.getElementById("q").focus();
			document.getElementById("q").select();
		}
	}

	onKeyUp = (e) => {
		if(this.props.location.pathname !== "/search"){
			this.props.history.push('/search')
		}
		if(e.keyCode === 8){
			if(this.props.store.toolsKeyword === ""){
				this.props.history.push('/')
			}
		}
	}

	@computed get fromColor(){
		if(this.props.store.profile.credits <= 0){
			return "bg-red-200 text-red-600"
		}
		if(this.props.store.profile.status === "trialing"){
			return ""
		}
		if(this.props.store.profile.status === "active"){
			return ""
		}
		if(this.props.store.profile.status === "incomplete"){
			return ""
		}
		return "bg-red-200 text-red-600"
	}

	render() {
		return (
						<>
							<Textarea readOnly name="copy-textarea" id="copy-textarea" value={this.props.store.copyToClipboardText}  />
							 <HeaderExpand>
										<Body className="px-4 py-4 md:px-28 md:py-8 lg:py-12 flex items-center flex-1">
											<div className="mr-4">
											<NavLink to="/"><Logo /></NavLink>
											</div>
												<div>
													<div className="text-4xl relative font-medium text-transparent bg-clip-text bg-gradient-to-r from-gray-700 to-gray-600 inline-block">stategy.ai<span className="font-normal "> Toolkit</span> 
														<div className="absolute top-0 ml-3 left-full bg-gradient-to-br from-gray-500 to-gray-500 text-white text-sm px-2 py-0.5 rounded-md font-normal hidden md:flex">gpt3</div>
													</div>
													<div className="hidden md:block text-xl text-transparent bg-clip-text bg-gradient-to-r from-gray-700 to-gray-500 inline-block">Optimized Ai Tools</div>
													<div className="flex">
														<div className={`items-center flex inline-flex ${this.props.store.profile.credits ? " bg-gray-100 text-gray-500" : " bg-red-100 text-red-500"} text-sm rounded-md px-3 py-1 font-medium my-2 mr-2`}>
															<DatabaseIcon className="w-4 h-4 mr-2" />{this.props.store.profile.credits}&nbsp;<span className="hidden lg:block">credits remain</span>
														</div>
													</div>
												</div>
										</Body>
								 </HeaderExpand>
								<div className="border-b border-gray-300 bg-white shadow-sm ">
										<div className="container flex mx-auto px-4 md:px-28 flex select-none">
														<NavLink to="/"
														 exact
														 tabIndex={-1}
														 onClick={()=>this.props.store.toolsKeyword = ""}
														 activeClassName="bg-gray-100 hover:bg-gray-200 text-gray-800 transition"
														 className="text-lg flex py-3 px-6 lg:py-4 lg:px-8 cursor-pointer hover:bg-gray-100 rounded-t-md font-medium transition items-center">
															 <IconDashboard className="w-7 h-7 lg:mr-4 transition" />
															 <div className="hidden lg:block">Tools</div>
															</NavLink>
														 
														<div className="relative text-gray-400 focus-within:text-green-500 flex flex-1 ">
															<label htmlFor="q" className="absolute inset-y-0 left-0 top-0 bottom-0 hidden md:flex items-center lg:pl-2 ">
																	<div type="submit" className="p-2 focus:outline-none focus:shadow-outline ">
																	<svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" viewBox="0 0 24 24" className="w-6 h-6 transition"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
																	</div>
															</label>
															<Input 
																type="search" 
																tabIndex={-1}
																id="q"
																name="q" 
																className="py-4 pl-4 md:pl-14 text-xl focus:outline-none focus:bg-white focus:text-gray-900 transition flex flex-1 w-full" placeholder="Search...  [Shortcut: Ctrl + K]" autoComplete="off" 
																value={this.props.store.toolsKeyword} 
																onChange={this.props.store.onChangeToolsKeyword} 
																onKeyUp={this.onKeyUp}
															/>
														</div>
														<NavLink to="/my-profile"
														 exact
														 tabIndex="-1"
														 activeClassName="bg-green-100 hover:bg-green-200 text-green-800 transition"
														 className={`text-lg flex py-3 px-6 xl:py-4 xl:px-8 cursor-pointer ${this.fromColor} hover:bg-gray-100 rounded-t-md font-medium transition items-center`}><UserCircleIcon className="w-7 h-7 lg:mr-4 transition" />
															<div className="hidden lg:block"> My Profile</div>
														</NavLink>


												</div>
												
								</div>
								{this.props.children}
						</>
			)
		}
}	

const Logo = () => (
	<svg
	xmlns="http://www.w3.org/2000/svg"
	fill="none"
	className="w-20 h-20"
	viewBox="0 0 1512 1532"
  >
	<path
	  fill="#000"
	  d="M459.5 209.9c-23.3 10-47.3 22.9-65.8 35.3-11.2 7.5-31.4 22.8-34.4 26l-2.1 2.3 121.1 121.8 121.2 121.9 11-9.2c24.8-20.8 51.9-38.2 84-54.2 8.8-4.3 16.1-8 16.2-8.2.1-.1-53.9-54.2-120-120.3L470.5 205.2l-11 4.7zM158.7 311.5c-1.6 4.9-4.7 26.2-5.8 39.5-1.2 15.3-.7 41.1 1.1 58 .6 5.2 2.4 16.2 4.1 24.4l3 14.9 94.9 95.6c52.2 52.6 126.3 127 164.7 165.3l69.8 69.8v-24.3c0-13.3.6-28.9 1.3-34.7 1.8-14.4 6.4-37.1 10.5-51.3l3.4-11.8-173.2-173.7C189.2 339.5 159.2 309.8 158.7 311.5zM877 450.6c-78.6 7.9-150.6 33-210.5 73.5-61.1 41.3-102.5 96.6-120 160.4-14 51.1-12 115.2 5 164.5 4.8 14 16 37 23.8 48.9 26.2 39.9 65.8 72.7 121.2 100.3 48.7 24.3 102.4 43 208 72.3 64.7 17.9 72.7 20.2 92.3 26.7 42.9 14.1 65.7 26 83.2 43.7 14.5 14.6 19.9 26.6 20.7 45.8.8 17.6-3 30.5-13.2 44.8-28.1 39.5-105.7 66.5-190.4 66.5-114-.1-219.9-39.4-317.8-117.9l-5.1-4.1-2.5 5.2c-1.3 2.9-20.7 44.9-43 93.4-22.4 48.4-40.7 88.5-40.7 89.1 0 2.4 26 24 45 37.5 136 96.4 323.3 133.5 495.3 98.2 133.7-27.5 231.3-91.8 276.7-182.4 17.7-35.1 26.6-70.1 28.9-113.4 5.6-102.3-33.9-182.7-119.5-243.3-61.3-43.4-152.5-78-271.9-103.3-59-12.4-80.7-17.9-102.2-26-36.8-13.8-59-32.6-66.4-56-3.5-11.2-4.5-23.2-2.6-32.5 2.7-13.1 8.4-23.3 19.1-34.1 17.6-17.7 40.7-29.7 73.1-37.9 22.5-5.6 35.4-7 66-7 56.8.1 107.3 8.2 160.5 25.7 36 11.8 78.9 30.9 109.2 48.6 1.5.9 3 1.3 3.3 1 .9-.8 84.7-185 84.3-185.3-5.4-4-50.9-28.7-72.3-39.1-75.4-36.6-139.6-54.8-223-63-23.1-2.3-95-2.8-114.5-.8zM2480.5 449.6c-1.6.2-8.6.9-15.5 1.5-40.9 3.6-91.2 16.3-129.8 32.8-27.6 11.8-46.3 22-72.7 39.6-58.8 39.2-107.3 91.7-145.5 157.5-82.9 142.9-100.2 336.9-44.5 500.9 36.9 108.5 103.1 197.9 189.5 255.6 69 46 144.2 70.4 230.1 74.5 80.4 3.8 161.9-12.6 230.4-46.3 44.2-21.8 75.4-44.3 111.3-80.3l21.2-21.3V1492h229V469h-229v130l-21.7-21.9c-21.8-21.8-35.8-33.7-57.3-48.9-63.1-44.4-141-71.6-224.4-78.2-11.7-.9-63.8-1.2-71.1-.4zm127 221.9c76.8 10.1 142.4 49.4 187.5 112.5 52.3 73.2 71.7 175 52 273-21.4 106.6-90.9 188.9-186.1 220.4-30.9 10.3-55.4 14-91.4 14-26.8-.1-40.2-1.3-63.5-6.1-118.1-23.9-205.1-115.4-226.9-238.3-16-89.9 1.3-181.2 47.4-250.5 45.3-68 115.6-112 198-123.9 26-3.8 59.1-4.2 83-1.1zM4338 449.6c-1.9.2-9.1.9-16 1.4-44.4 3.8-94 15.3-138 32-100.5 38.1-188.2 108.6-248.5 200-15.2 23-23.3 37.2-35.1 61.5-48.2 99.2-64.4 211.5-47.4 327.5 13.8 93.4 51.1 178.4 109.5 249.2 13.6 16.6 44.9 48 61.4 61.7 34.4 28.7 65 48.5 105.6 68.6 181.4 89.7 407.8 80 600-25.8 19.9-10.9 54.6-32.4 56-34.6.5-.9-87.2-165.9-89.5-168.4-.4-.5-4.8 1.8-9.6 5-57.3 38.4-133.5 66.6-208.4 77.2-29.8 4.2-36 4.6-73.5 4.6-39.7 0-49.3-.8-76.5-6.1-124.6-24.5-212-107.3-241.6-228.9-1.3-5.5-2.4-10.6-2.4-11.3 0-.9 78.1-1.2 387.4-1.2h387.4l.6-4.3c2.8-19.5 5.6-62.2 5.6-84.7 0-147-58-286.9-160.4-386.7-82.4-80.4-182.9-126.9-294.1-136.3-10.8-.9-63.9-1.2-72.5-.4zm65 201.8c58.3 9.3 107.5 32.4 149.5 70.3 39.1 35.2 69.6 86.6 83.1 139.5 2.8 11.1 6.4 28.2 6.4 30.4 0 1.2-41.6 1.4-280 1.4-242.4 0-280-.2-280-1.4 0-.8 1.3-6.6 2.9-12.9 25.2-97.5 87.1-172.7 172.2-209 21.2-9.1 46.7-15.9 70.9-19.2 14.6-1.9 60.5-1.4 75 .9zM5352 450.1c-134 10.9-248.6 73.3-327 178.1-74.5 99.4-109.2 230.5-97.4 367.8 12.4 144.9 78.9 271.8 184 351.2 66 49.8 140.9 78.6 230.4 88.5 22.7 2.5 82.5 2.5 104.5 0 84.7-9.6 152.2-35.9 209-81.4 13.4-10.8 34.3-31.5 44.7-44.3 7.8-9.8 21.7-30.2 27.6-40.6l3.3-5.9-.5 25.5c-1.7 86.9-24.7 161.9-67.3 218.9-11.4 15.2-36.4 40.2-51.4 51.4-46.3 34.5-101.1 53.8-169.4 59.8-16.7 1.4-59.2 1.4-76 0-75.4-6.5-148.1-28-206.8-61.2-14.3-8.1-39.1-24.8-50.7-34.2-6.9-5.5-10.7-8.1-11.2-7.3-.4.7-20.3 33.1-44.3 72.1l-43.6 71 4.3 3.8c73.7 64.9 182.2 109.5 305.8 125.7 68.6 8.9 140.5 8.9 204-.1 102.4-14.5 194.9-54.6 265.6-115 101.9-87.1 159.4-212.4 169.4-368.9.6-11 1-152.7 1-426.3V469h-218l-.2 76.2-.3 76.3-2-3.4c-5.5-9.2-18.1-27.5-24.1-35.1-9.4-11.7-31.7-33.8-44.4-43.9-22-17.5-40.9-29.5-68.3-43.1-50.1-25-105.3-40.2-165.5-45.5-18.2-1.6-67.3-1.8-85.2-.4zm126 200.5c64.7 7.4 122.1 34.3 167.4 78.4 22.8 22.2 39.1 44.1 53.2 71.4 15.2 29.5 24.4 57.8 29.5 90.8 2.7 17.2 3.7 55.9 2 76.1-8.1 95.1-52.8 175.2-125.1 223.7-62.1 41.6-143.6 56.6-222 40.9-52.4-10.5-102.6-37.5-140.5-75.4-45.6-45.6-73.7-105.6-82.1-175-2.3-18.8-2.3-56 0-74.5 6.8-55.7 26.4-106 57.5-147.4 45.7-60.8 114-99.7 191.1-109 16.1-1.9 52.3-2 69 0zM6008 469.5c0 .3 99.4 208.3 221 462.1l221 461.6-13.5 26.7c-17.8 35.2-24.7 47.5-37 66.1-23.5 35.5-46 56.6-77 72-28.9 14.4-59.4 21.3-100.7 22.7l-17.8.6v215l20.3-.7c82.2-2.8 138.9-15.6 194.1-43.9 19.8-10.1 32.8-18.2 49.8-30.9 51.8-38.8 92.4-89.6 126.4-158.3 6.6-13.4 107.9-243.9 242.8-552.5 127.3-291.2 232.5-532.1 233.9-535.3l2.6-5.7h-231.4l-111.4 269.8c-105.8 256.1-124.1 301.4-160.2 395.9-8.4 22.2-15.5 40.3-15.8 40.3-.3 0-1.9-3.5-3.5-7.8-6.7-18.1-34.6-87.5-45.1-112-6.3-14.7-68.1-152.5-137.4-306.2l-126-279.5-117.6-.3c-64.6-.1-117.5 0-117.5.3z"
	></path>
  </svg>
	
)

const Input = styled.input`
	
`

const Textarea = styled.textarea`
	position: fixed;
	right:-9990px;
	top:-9990px;
`

// transition:ease-in-out 0.5s margin-top;
// 	transition-delay:0.05s;

const SuperHeader = styled.div`
	height:150px;
	background:white;
	margin-top:${props => props.active ? "0px" : "-150px" };
	display:${props => props.hidden ? "hidden" : "flex" };
	background-image:url(${require('./pattern-dots.svg').default});
	background-size:auto 50%;
	background-position: 20px 20px;
	background-repeat:no-repeat;
	position:relative;
	
`


	
export default withRouter(SidebarCompontent)